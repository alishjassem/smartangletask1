package smart.angle.task1.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import smart.angle.task1.di.PreferenceInfo;


public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_LAST_INDEX = "PREF_KEY_LAST_INDEX";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public int getLastIndex() {
        return mPrefs.getInt(PREF_KEY_LAST_INDEX, 0);
    }

    @Override
    public void setLastIndex(int index) {
        mPrefs.edit().putInt(PREF_KEY_LAST_INDEX, index).apply();
    }
}
