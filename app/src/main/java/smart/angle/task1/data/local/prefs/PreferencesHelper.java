package smart.angle.task1.data.local.prefs;

public interface PreferencesHelper {

    int getLastIndex();

    void setLastIndex(int index);

}
