package smart.angle.task1.data;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import smart.angle.task1.data.local.prefs.PreferencesHelper;

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public AppDataManager(Context context, PreferencesHelper preferencesHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
    }

    @Override
    public int getLastIndex() {
        return mPreferencesHelper.getLastIndex();
    }

    @Override
    public void setLastIndex(int index) {
        mPreferencesHelper.setLastIndex(index);
    }
}
