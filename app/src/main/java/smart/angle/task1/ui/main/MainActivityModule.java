package smart.angle.task1.ui.main;

import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    AdapterMessages provideAdapterMessages() {
        return new AdapterMessages();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(MainActivity mainActivity) {
        return new LinearLayoutManager(mainActivity);
    }
}
