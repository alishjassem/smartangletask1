package smart.angle.task1.ui.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import smart.angle.task1.databinding.ItemMessageBinding;
import smart.angle.task1.ui.base.BaseViewHolder;

public class AdapterMessages extends RecyclerView.Adapter<BaseViewHolder> {

    private List<String> mMessagesList;
    private int lastCheckedPosition = 0;
    private MessagesAdapterListener mListener;

    @Override
    public int getItemCount() {
        return mMessagesList.size();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMessageBinding itemMessageBinding = ItemMessageBinding.
                inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MessageViewHolder(itemMessageBinding);
    }

    public void setArray(String[] messagesList) {
        mMessagesList = Arrays.asList(messagesList);
        notifyDataSetChanged();
    }

    public String getFilterByPosition(int position) {
        return mMessagesList.get(position);
    }

    public int getLastCheckedPosition() {
        return lastCheckedPosition;
    }

    public void setListener(MessagesAdapterListener listener) {
        this.mListener = listener;
    }

    public void addItem(String s) {
        mMessagesList.add(s);
        notifyDataSetChanged();
    }

    public interface MessagesAdapterListener {
        void onItemClick(int position);
    }

    public class MessageViewHolder extends BaseViewHolder implements MessageItemViewModel.MessageItemViewModelListener {

        private final ItemMessageBinding mBinding;

        public MessageViewHolder(ItemMessageBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            MessageItemViewModel mMenuItemViewModel = new MessageItemViewModel(mMessagesList.get(position), this);
            mBinding.setViewModel(mMenuItemViewModel);
            mBinding.setPosition(position);
            mBinding.radio.setChecked(position == lastCheckedPosition);
            mBinding.radio.setOnClickListener(v -> {
                int copyOfLastCheckedPosition = lastCheckedPosition;
                lastCheckedPosition = getAdapterPosition();
                notifyItemChanged(copyOfLastCheckedPosition);
                notifyItemChanged(lastCheckedPosition);
            });

        }

        @Override
        public void onItemClick(int position) {
            int copyOfLastCheckedPosition = lastCheckedPosition;
            lastCheckedPosition = getAdapterPosition();
            notifyItemChanged(copyOfLastCheckedPosition);
            notifyItemChanged(lastCheckedPosition);
            mListener.onItemClick(position);
        }
    }
}
