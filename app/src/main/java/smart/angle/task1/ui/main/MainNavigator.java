package smart.angle.task1.ui.main;

public interface MainNavigator {

    void handleError(Throwable throwable);

    void showMessageIndex();
}
