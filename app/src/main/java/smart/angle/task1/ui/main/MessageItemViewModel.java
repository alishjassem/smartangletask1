package smart.angle.task1.ui.main;

import androidx.databinding.ObservableField;

public class MessageItemViewModel {

    public final MessageItemViewModelListener mListener;
    public final ObservableField<String> title;

    public MessageItemViewModel(String title, MessageItemViewModelListener listener) {
        this.mListener = listener;
        this.title = new ObservableField<>(title);
    }

    public void onItemClick(int position) {
        mListener.onItemClick(position);
    }

    public interface MessageItemViewModelListener {

        void onItemClick(int position);
    }

}
