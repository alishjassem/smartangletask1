package smart.angle.task1.ui.main;

import smart.angle.task1.data.DataManager;
import smart.angle.task1.ui.base.BaseViewModel;
import smart.angle.task1.utils.rx.SchedulerProvider;


public class MainViewModel extends BaseViewModel<MainNavigator> {

    private static final String TAG = "MainViewModel";

    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
