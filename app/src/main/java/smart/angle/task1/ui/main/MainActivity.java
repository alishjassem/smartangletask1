package smart.angle.task1.ui.main;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import smart.angle.task1.BR;
import smart.angle.task1.R;
import smart.angle.task1.ViewModelProviderFactory;
import smart.angle.task1.databinding.ActivityMainBinding;
import smart.angle.task1.ui.base.BaseActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel>
        implements MainNavigator, AdapterMessages.MessagesAdapterListener,
        HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProviderFactory factory;
    @Inject
    AdapterMessages mAdapterMessages;
    @Inject
    LinearLayoutManager mLinearLayoutManager;
    private ActivityMainBinding mActivityMainBinding;
    private MainViewModel mMainViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    public MainViewModel getViewModel() {
        mMainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        return mMainViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void showMessageIndex() {
        Toast.makeText(this, "index = " + mAdapterMessages.getLastCheckedPosition(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = getViewDataBinding();
        mMainViewModel.setNavigator(this);
        mAdapterMessages.setListener(this);
        setUp();
    }

    private void setUp() {
        String[] messages = getResources().getStringArray(R.array.messages);
        mAdapterMessages.setArray(messages);
        mActivityMainBinding.recyclerMessages.setAdapter(mAdapterMessages);
    }

    @Override
    public void onItemClick(int position) {

    }
}
