package smart.angle.task1.di.builder;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import smart.angle.task1.ui.main.MainActivity;
import smart.angle.task1.ui.main.MainActivityModule;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {
            MainActivityModule.class
    })
    abstract MainActivity bindMainActivity();
}
